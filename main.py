from flask import Flask, render_template, request
import openai

openai.organization = "org-F1sgUQlKDircLkFXB0WvLEcV"
openai.api_key = "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"
app = Flask(__name__)


@app.route("/", methods=['POST', 'GET'])
def index():
    #Cas ou le formulaire est validé (POST)
    if request.method == 'POST':
        donnees = request.form
        prompt = donnees.get('prompt')
        response = openai.Image.create(
            prompt=prompt,
            n=1,
            size="512x512"
        )
        image_url = response['data'][0]['url']
        return render_template("index.html",image_url=image_url)
    return render_template("index.html")


if __name__ == "__main__":
    app.run()
